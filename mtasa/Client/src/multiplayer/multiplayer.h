#ifndef MULTIPLAYER_H
#define MULTIPLAYER_H

#include <vector>
#include "localplayer.h"
#include "multiplayer_keysync.h"
#include "multiplayer_shotsync.h"
#include "multiplayer_hooksystem.h"

#define DEFAULT_FAR_CLIP_DISTANCE   ( 150.0f )
#define DEFAULT_NEAR_CLIP_DISTANCE  ( 0.3f )

#define MAX_STREAMING_MEMORY 0x40000000

enum eRadioStationID
{
    UNKNOWN = 0,
    Playback_FM,
    K_Rose,
    K_DST,
    BOUNCE_FM,
    SF_UR,
    RLS,
    RADIO_X,
    CSR_1039,
    K_JAH_WEST,
    Master_Sounds,
    WCTR,
};

class multiplayer
{
    friend class offsets;
    friend class CFont;

public:
    static bool                m_bSuspensionEnabled;
    static std::vector<char>   m_PlayerImgCache;
    static bool                m_bEnabledLODSystem;
    static bool                m_bEnabledAltWaterOrder;
    static bool                m_bEnabledClothesMemFix;
    static float               m_fAircraftMaxHeight;
    static float               m_fAircraftMaxVelocity;
    static float               m_fAircraftMaxVelocity_Sq;
    static bool                m_bHeatHazeEnabled;
    static bool                m_bHeatHazeCustomized;
    static float               m_fNearClipDistance;
    static eAnimGroup          m_dwLastStaticAnimGroupID;
    static eAnimID             m_dwLastStaticAnimID;
    static DWORD               m_dwLastAnimArrayAddress;
    static float               m_fFarClipDistance;
    static bool                m_bAllowSinglePeds;
    static bool                m_bAllowSingleCars;
    static bool                m_bRenderInited;

     static uintptr_t           HOOKPOS_CHud_Draw;
     static uintptr_t           HOOKPOS_RenderQueue_ProcessCommand;
     static uintptr_t           FUNC_CFont_AsciiToGxtChar;
     static uintptr_t           FUNC_CFont_PrintString;
     static uintptr_t           FUNC_CFont_SetColor;
     static uintptr_t           FUNC_CFont_SetDropColor;
     static uintptr_t           FUNC_CFont_SetEdge;
     static uintptr_t           FUNC_CFont_SetJustify;
     static uintptr_t           FUNC_CFont_SetScale;
     static uintptr_t           FUNC_CFont_SetScaleXY;
     static uintptr_t           FUNC_CFont_SetOrientation;
     static uintptr_t           FUNC_CFont_SetFontStyle;
     static uintptr_t           FUNC_CFont_SetProportional;
     static uintptr_t           FUNC_CFont_SetRightJustifyWrap;
     static uintptr_t           FUNC_CFont_SetBackground;
     static uintptr_t           FUNC_CFont_SetBackgroundColor;
     static uintptr_t           FUNC_CFont_SetWrapx;
     static uintptr_t           FUNC_CFont_SetCentreSize;
     static uintptr_t           FUNC_CFont_SetDropShadowPosition;
     static uintptr_t           FUNC_CFont_GetHeight;
     static uintptr_t           FUNC_CFont_GetStringWidth;
     static uintptr_t           FUNC_CFont_GetTextRect;
     static uintptr_t           FUNC_CFont_SetScaleY;
     static uintptr_t           FUNC_CFont_SetScaleX;
     static uintptr_t           FUNC_CFont_RenderFontBuffer;
     static uintptr_t           FUNC_OperatorNew;
     static uintptr_t           VAR_WORLD_PLAYERS;
     static uintptr_t           ADDR_SHADOWCRASH1;
     static uintptr_t           ADDR_SHADOWCRASH2;
     static uintptr_t           ADDR_SHADOWCRASH3;
     static uintptr_t           ADDR_COLLISION_POOL1;
     static uintptr_t           ADDR_COLLISION_POOL2;
     static uintptr_t           HOOKPOS_RwCameraSetNearClipPlane;
     static uintptr_t           HOOKPOS_RwCameraSetFarClipPlane;
     static uintptr_t           VAR_RS_GLOBAL;
     static uintptr_t           FUNC_RwCameraBeginUpdate;
     static uintptr_t           FUNC_RwCameraEndUpdate;
     static uintptr_t           FUNC_RwCameraShowRaster;
     static uintptr_t           FUNC_RwRasterCreate;
     static uintptr_t           FUNC_RwRasterDestroy;
     static uintptr_t           FUNC_RwRasterGetOffset;
     static uintptr_t           FUNC_RwRasterGetNumLevels;
     static uintptr_t           FUNC_RwRasterSubRaster;
     static uintptr_t           FUNC_RwRasterRenderFast;
     static uintptr_t           FUNC_RwRasterRender;
     static uintptr_t           FUNC_RwRasterRenderScaled;
     static uintptr_t           FUNC_RwRasterPushContext;
     static uintptr_t           FUNC_RwRasterPopContext;
     static uintptr_t           FUNC_RwRasterGetCurrentContext;
     static uintptr_t           FUNC_RwRasterClear;
     static uintptr_t           FUNC_RwRasterClearRect;
     static uintptr_t           FUNC_RwRasterShowRaster;
     static uintptr_t           FUNC_RwRasterLock;
     static uintptr_t           FUNC_RwRasterUnlock;
     static uintptr_t           FUNC_RwRasterLockPalette;
     static uintptr_t           FUNC_RwRasterUnlockPalette;
     static uintptr_t           FUNC_RwImageCreate;
     static uintptr_t           FUNC_RwImageDestroy;
     static uintptr_t           FUNC_RwImageAllocatePixels;
     static uintptr_t           FUNC_RwImageFreePixels;
     static uintptr_t           FUNC_RwImageCopy;
     static uintptr_t           FUNC_RwImageResize;
     static uintptr_t           FUNC_RwImageApplyMask;
     static uintptr_t           FUNC_RwImageMakeMask;
     static uintptr_t           FUNC_RwImageReadMaskedImage;
     static uintptr_t           FUNC_RwImageRead;
     static uintptr_t           FUNC_RwImageWrite;
     static uintptr_t           FUNC_RwImageSetFromRaster;
     static uintptr_t           FUNC_RwRasterSetFromImage;
     static uintptr_t           FUNC_RwRasterRead;
     static uintptr_t           FUNC_RwRasterReadMaskedRaster;
     static uintptr_t           FUNC_RwImageFindRasterFormat;
     static uintptr_t           FUNC_RwTextureCreate;
     static uintptr_t           FUNC__rwStreamInitialize;
     static uintptr_t           FUNC_RwStreamOpen;
     static uintptr_t           FUNC_RwStreamClose;
     static uintptr_t           FUNC_RwStreamRead;
     static uintptr_t           FUNC_RwStreamWrite;
     static uintptr_t           FUNC_RwStreamSkip;
     static uintptr_t           FUNC_RwIm2DGetNearScreenZ;
     static uintptr_t           FUNC_RwIm2DGetFarScreenZ;
     static uintptr_t           FUNC_RwRenderStateGet;
     static uintptr_t           FUNC_RwRenderStateSet;
     static uintptr_t           FUNC_RwIm2DRenderLine;
     static uintptr_t           FUNC_RwIm2DRenderTriangle;
     static uintptr_t           FUNC_RwIm2DRenderPrimitive;
     static uintptr_t           FUNC_RwIm2DRenderIndexedPrimitive;
     static uintptr_t           FUNC_RtPNGImageWrite;
     static uintptr_t           FUNC_RtPNGImageRead;
     static uintptr_t           FUNC_ExecuteScriptBuf;
     static uintptr_t           FUNC_TextureDatabaseRuntime_GetTexture;
private:
    localplayer* g_pLocalPlayer;
public:
    multiplayer();
    ~multiplayer();
    void initRender();
    void InitHooksAndPatches();
    void Init_13();
    void InitHooks_13();
    void InitMemoryCopies_13();
    void InitHooks_CrashFixHacks();
    void InitHooks_Weapons();
    void InitHooks_KeySync();
    void DisableSinglePeds(bool bDisable);
    void DisableSingleCars(bool bDisable);
    void SetFarClipDistance(float fDistance);
    void SetNearClipDistance(float fDistance);
    float GetNearClipDistance();
    float GetFarClipDistance();
    localplayer* getLocalPlayer() { return g_pLocalPlayer; };
};

#endif // MULTIPLAYER_H