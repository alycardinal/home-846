/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "../main.h"
#include "multiplayer.h"

extern ARMHook *pGTASAHook;

void HookInstall(uintptr_t addr, uintptr_t func, uintptr_t *orig)
{
    pGTASAHook->installHook(addr, func, orig);
}

void PLTHookInstall(uintptr_t addr, uintptr_t func, uintptr_t *orig)
{
    InstallPLTHook(addr, func, orig);
}

void MTDHookInstall(uintptr_t addr, uintptr_t func)
{
    InstallMethodHook(addr, func);
}