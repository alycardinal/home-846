#ifndef STDINC_H
#define STDINC_H

#define ARM_TRAMPOLINE_START_18     0x180694
#define ARM_TRAMPOLINE_START_20     0x1A9E0C
#define ARM_TRAMPOLINE_SIZE         0x1A36

#define CUSTOM_BUILD_INFO   0x307F014D
#define OFFICAL_BUILD_INFO  0x3DAF20BD
#define AC_STARTED_INFO     0xE3717ABD

#define BUILD_TYPE_UNKNOWN  22
#define BUILD_TYPE_OFFICAL  31
#define BUILD_TYPE_CUSTOM   44

#define MTAEXPORT extern "C" __attribute__ ((visibility ("default")))

#include <jni.h>
#include <dlfcn.h>
#include <link.h>
#include <sys/mman.h>
#include <pthread.h>
#include <cstdlib>
#include <vector>
#include <list>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <string.h>
#include <stdio.h>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <cstdio>
#include <cstring>

extern uintptr_t g_GTASAHandle;
extern uintptr_t g_ClientAC;
extern uintptr_t g_SCAndHandle;
extern uintptr_t g_ImmEmulatorHandle;
extern uintptr_t g_NetworkModuleHandle;

#define SA(dest) (g_GTASAHandle + dest)
#define MTA_AC(dest) (g_ClientAC + dest)
#define SC(dest) (g_SCAndHandle + dest)
#define IMM(dest) (g_ImmEmulatorHandle + dest)
#define NM(dest) (g_NetworkModuleHandle + dest)

namespace arm {
    template <unsigned int address, typename... Args>
    void Call(Args... args) {
        reinterpret_cast<void(__cdecl *)(Args...)>(address)(args...);
    }

    template <typename Ret, unsigned int address, typename... Args>
    Ret CallAndReturn(Args... args) {
        return reinterpret_cast<Ret(__cdecl *)(Args...)>(address)(args...);
    }

    template <unsigned int address, typename C, typename... Args>
    void CallMethod(C _this, Args... args) {
        reinterpret_cast<void(__thiscall *)(C, Args...)>(address)(_this, args...);
    }

    template <typename Ret, unsigned int address, typename C, typename... Args>
    Ret CallMethodAndReturn(C _this, Args... args) {
        return reinterpret_cast<Ret(__thiscall *)(C, Args...)>(address)(_this, args...);
    }
};

#include "../../../version.h"
#include "clientbase.h"
#include "../../../game_sa/game_sa.h"

#include "../../../../vendor/obfuscator/str_obfuscate.hpp"
#include "../../../../vendor/obfuscator/str_obfuscator.hpp"

// import utilities
#include "../../../utilities/config.h"
#include "../../../utilities/armhook.h"
#include "../../../utilities/utilities.h"
#include "../../../utilities/log.h"
#include "../../../utilities/crashdump.h"
#include "../../../utilities/javawrapper.h"

#endif // STDINC_H
