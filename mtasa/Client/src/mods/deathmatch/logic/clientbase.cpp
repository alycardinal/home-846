/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "stdinc.h"
#include "core/core.h"
#include "multiplayer/multiplayer_init.h"

ARMHook 	*pGTASAHook 	= 0;
uint8_t      g_buildType    = BUILD_TYPE_UNKNOWN;

void *mainThread(void *p)
{	
	pthread_exit(0);
}

void *uiThread(void *p)
{	
    Config::Load("mtasa.ini");
    static uint usWindowCutDisable = Config::GetValueInt("mtasa-cfg-10", "windowCutDisable");
    static uint usFullScreenEnable = Config::GetValueInt("mtasa-cfg-10", "fullScreenEnable");
    Config::Unload();

    while(true)
    {
        if(usFullScreenEnable) 
        {
            javawrapper::hideNavBar();

            if(usWindowCutDisable) {
                javawrapper::hideLayoutCut();
            }
        }
    }

	pthread_exit(0);
}

uint8_t anticheat::getBuildType()
{
    return g_buildType;
}

void anticheat::update()
{
    LOG("[ARM-AC] Checking version..");

    uintptr_t ac_result = 0;

	if(g_ClientAC)
	{
        uintptr_t getACResult = utilities::findMethod("libhmta.so", "_ZN6ACInfo9getACInfoEv");
        uintptr_t validateACKey = utilities::findMethod("libhmta.so", "_ZN6ACInfo13validateACKeyEPKc");

        if(getACResult <= 0 || validateACKey <= 0 || getACResult > 0xFFFFFF || validateACKey > 0xFFFFFF) 
        {
            LOG("[ARM-AC] Error: invalid request!");
            throw -1;
			return;
        }

		ac_result = ((uintptr_t (*)())(MTA_AC(getACResult)))();

		if(ac_result == 0x00 || ac_result != AC_STARTED_INFO)
		{
			LOG("[ARM-AC] Error: invalid result!");
            throw -1;
			return;
		}
        else
        {
            ((void (*)(const char*))(MTA_AC(validateACKey)))(CLIENT_AC_KEY);
            ac_result = ((uintptr_t (*)())(MTA_AC(getACResult)))();
            g_buildType = (ac_result != OFFICAL_BUILD_INFO) ? g_buildType = BUILD_TYPE_CUSTOM : g_buildType = BUILD_TYPE_OFFICAL;
            return;
        }
	}
    
	LOG("[ARM-AC] Error: no library found!");
    throw -1;
	return;
}

void unloadClient()
{
    javawrapper::uninitialize();

    if(pGTASAHook)
	{
		delete pGTASAHook;
		pGTASAHook = 0;
	}
}

void startClient()
{
    anticheat::update();

    switch(anticheat::getBuildType()) 
    {
        default:
        case BUILD_TYPE_CUSTOM: {
            LOG("[ARM-AC] Build type: CUSTOM");
            break;
        }

        case BUILD_TYPE_OFFICAL: {
            LOG("[ARM-AC] Build type: OFFICIAL");
            break;
        }
    }

    // StartGameScreen::OnNewGameCheck
	(( void (*)())(SA(utilities::findMethod("libGTASA.so", "_ZN15StartGameScreen14OnNewGameCheckEv"))))();
}

void setupClient()
{
    // STORAGE setups
	utilities::setClientStorage(
		"/storage/emulated/0/Android/data/com.rockstargames.gtasa/files/"
		);

    // ARM setups
	pGTASAHook = new ARMHook(g_GTASAHandle, core::getGame()->FindGameVersion() == VERSION_A_108 ? ARM_TRAMPOLINE_START_18 : ARM_TRAMPOLINE_START_20, ARM_TRAMPOLINE_SIZE); 
	InitGlobalHooksAndPatches();

    // client main thread
	pthread_t thread;
	pthread_create(&thread, 0, mainThread, 0);	

    // android UI thread
	pthread_t ui_thread;
	pthread_create(&ui_thread, 0, uiThread, 0);	
}

void CClient::ProcessCommand(const char* szArguments)
{
    LOG("Client command executed: %s", szArguments);

    if (szArguments && szArguments[0] != '\0')
    {
        if (strcmp(szArguments, "setup") == 0)
        {
            setupClient();
        }
        else if (strcmp(szArguments, "unload") == 0)
        {
            unloadClient();
        }
        else if (strcmp(szArguments, "start") == 0)
        {
            startClient();
        }
    }
}