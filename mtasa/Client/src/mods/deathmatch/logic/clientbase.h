#ifndef CCLIENT_H
#define CCLIENT_H

class anticheat {
public:
    static void update();
    static uint8_t getBuildType();
};

class CClient
{
private:
    /* data */
public:
    static void ProcessCommand(const char* szArguments);
};

#endif // CCLIENT_H