#include "../main.h"

bool CStreamingInfo::GetCdPosnAndSize(unsigned int *CdPosn, unsigned int *CdSize)
{
    return ((bool (*)(CStreamingInfo*, unsigned int *, unsigned int *))(SA(utilities::findMethod("libGTASA.so", "_ZN14CStreamingInfo16GetCdPosnAndSizeERjS0_"))))(this, CdPosn, CdSize);
}

void CStreaming::RequestModel(int modelId, eStreamingFlags flags)
{   
    ((void (*)(int, eStreamingFlags))(SA(utilities::findMethod("libGTASA.so", "_ZN10CStreaming12RequestModelEii"))))(modelId, flags);
}

void CStreaming::LoadAllRequestedModels(bool bOnlyPriorityRequests)
{
    ((void (*)(bool))(SA(utilities::findMethod("libGTASA.so", "_ZN10CStreaming22LoadAllRequestedModelsEb"))))(bOnlyPriorityRequests);
}

void CStreaming::RemoveModel(int Modelindex)
{
    ((void (*)(int))(SA(utilities::findMethod("libGTASA.so", "_ZN10CStreaming11RemoveModelEi"))))(Modelindex);
}