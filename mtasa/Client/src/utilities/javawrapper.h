#ifndef JAVAWRAPPER_H
#define JAVAWRAPPER_H

#include <jni.h>

#define EXCEPTION_CHECK(env) \
	if ((env)->ExceptionCheck()) \ 
	{ \
		(env)->ExceptionDescribe(); \
		(env)->ExceptionClear(); \
		return; \
	}

class javawrapper
{
private:
    static JavaVM* globalVM;
    static jobject activity;
    static jmethodID s_DisableUIStuff;
    static jmethodID s_HideLayoutCut;
    static jmethodID s_HideNavBar;
    static jmethodID s_ShowMessage;
    static jmethodID s_MakeDialog;
public:
    static JNIEnv* getEnv();
    static void setGlobalVM(JavaVM* vm);
    static void initialize(JNIEnv*, jobject);
    static void uninitialize();

    static void disableUIStuff();
    static void hideLayoutCut();
    static void hideNavBar();
    static void showMessage(const char* szMsg);
    static void makeDialog(const char* szTitle, const char* szMsg, const char* szButton1, const char* szButton2);
};

#endif // JAVAWRAPPER_H