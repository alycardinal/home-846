/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

/* ========== DESTROY THIS SHIT AFTER ========== */

#include "main.h"
#include "../core/core.h"
#include "gui.h"
#include "window.h"

extern window *pMainWindow;

char keyboard::inputBuffer[512] = { 0 };
bool keyboard::g_bDraw = false;
bool keyboard::g_bShift = false;

#define KEYBOARD_TYPE_ENG 0
#define KEYBOARD_TYPE_RUS 1
#define KEYBOARD_TYPE_NUM 2

uint keyboard::g_iType = KEYBOARD_TYPE_ENG;
bool keyboard::g_bSpecialSym = false;

#define MAX_KEYBOARD_BUFFER_SIZE 512

uint32_t dwLastTickCharAdd = utilities::getTickCount();
uint32_t dwLastTickCharDel = utilities::getTickCount();

std::vector<std::string> vecMessages;
int m_iMessageHistoryPoint = 0;
bool bHistoryMessagesAllow = false;
bool bFirstInit = false;

void keyboard::setVisibility(bool s)
{
    if(!bFirstInit)
    {
        keyboard::allowHistory(true);
        vecMessages.clear();
        bFirstInit = true;
    }

    keyboard::g_bDraw = s;
}

void keyboard::allowHistory(bool s)
{
    bHistoryMessagesAllow = s;
}

void keyboard::saveHistory(const std::string& msg)
{
	if (msg.size() == 0) { 
        return;
    }

	vecMessages.insert(vecMessages.begin(), msg);
}

void keyboard::scrollUpHistory()
{
	if (!vecMessages.size())
	{
		m_iMessageHistoryPoint = 0;
		return;
	}

	m_iMessageHistoryPoint++;

	if (m_iMessageHistoryPoint >= vecMessages.size() + 1)
	{
		m_iMessageHistoryPoint = vecMessages.size();
		return;
	}

	keyboard::setText((char*)vecMessages[m_iMessageHistoryPoint - 1].c_str());
}

void keyboard::scrollDownHistory()
{
	m_iMessageHistoryPoint--;

	if (m_iMessageHistoryPoint <= 0)
	{
		m_iMessageHistoryPoint = 0;
		return;
	}

	keyboard::setText((char*)vecMessages[m_iMessageHistoryPoint - 1].c_str());
}

void keyboard::pushShift()
{
    keyboard::g_bShift ^= true;
}

void keyboard::addChar(char t)
{
    if(utilities::getTickCount() - dwLastTickCharAdd < 60) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    int iFree = -1;
    
    for(int i = 0; i < MAX_KEYBOARD_BUFFER_SIZE; i++) 
    {
        if(!keyboard::inputBuffer[i]) 
        {
            iFree = i;
            break;
        }
    }

    if(iFree != -1) 
    {
        keyboard::inputBuffer[iFree] = t;

        dwLastTickCharAdd = utilities::getTickCount();
    }
}

void keyboard::removeChar()
{
    if(utilities::getTickCount() - dwLastTickCharDel < 120) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    for(int i = MAX_KEYBOARD_BUFFER_SIZE-1; i >= 0; i--)
    {
        if(keyboard::inputBuffer[i]) 
        {
            keyboard::inputBuffer[i] = '\0';
            dwLastTickCharDel = utilities::getTickCount();
            break;
        }
    }
}

void keyboard::setText(char* text)
{
    strncpy(keyboard::inputBuffer, text, MAX_KEYBOARD_BUFFER_SIZE);
}

char* keyboard::getText()
{
    return keyboard::inputBuffer;
}

void keyboard::draw()
{
    if(!keyboard::g_bDraw) {
        return;
    }

    static uint m_lastType = KEYBOARD_TYPE_ENG;

    float m_fWidth = 160.0f; 
	float m_fHeight = 130.0f;
    float m_fSame = 5.0f;

    ImGui::SetNextWindowBgAlpha(0.3f);

	ImGuiIO& io = ImGui::GetIO();

	ImGui::Begin("keyboard", nullptr, 
        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar);

    char keys_rus[] = {
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�', 
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�',
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�',
        keyboard::g_bShift ? '�' : '�', keyboard::g_bShift ? '�' : '�'
    };

    char keys_eng[] = {
        keyboard::g_bShift ? 'Q' : 'q', keyboard::g_bShift ? 'W' : 'w', 
        keyboard::g_bShift ? 'E' : 'e', keyboard::g_bShift ? 'R' : 'r', 
        keyboard::g_bShift ? 'T' : 't', keyboard::g_bShift ? 'Y' : 'y', 
        keyboard::g_bShift ? 'U' : 'u', keyboard::g_bShift ? 'I' : 'i', 
        keyboard::g_bShift ? 'O' : 'o', keyboard::g_bShift ? 'P' : 'p', 
        keyboard::g_bShift ? 'A' : 'a', keyboard::g_bShift ? 'S' : 's', 
        keyboard::g_bShift ? 'D' : 'd', keyboard::g_bShift ? 'F' : 'f', 
        keyboard::g_bShift ? 'G' : 'g', keyboard::g_bShift ? 'H' : 'h', 
        keyboard::g_bShift ? 'J' : 'j', keyboard::g_bShift ? 'K' : 'k', 
        keyboard::g_bShift ? 'L' : 'l', keyboard::g_bShift ? 'Z' : 'z', 
        keyboard::g_bShift ? 'X' : 'x', keyboard::g_bShift ? 'C' : 'c', 
        keyboard::g_bShift ? 'V' : 'v', keyboard::g_bShift ? 'B' : 'b', 
        keyboard::g_bShift ? 'N' : 'n', keyboard::g_bShift ? 'M' : 'm'
    };

    char keys_num[] = {
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
        '@', '#', '$', '_', '&', '-', '+', '(', ')', '/',
        '*', '"', '\'', ':', ';', '!', '?'
    };

    char keys_num_special[] = {
        '~', '`', '|', '^', '=', '{', '}', '\\', '%', '[', ']'
    };

    if(keyboard::g_iType == KEYBOARD_TYPE_ENG)
    {
        for (int i = 0; i < sizeof(keys_eng); i++)
        {
            float fButtonSizeX = io.DisplaySize.x / 11;
            m_fWidth = fButtonSizeX;

            bool bSizeAmmersive = false;
            bool bSizeFaster = false;

            if(!keys_eng[i]) {
                continue;
            }

            char key_buf[0xFF] = { 0 };
            sprintf(key_buf, "%c", keys_eng[i]);

            if(tolower(keys_eng[i]) == 'q' && !bSizeFaster)
            {
                m_fWidth += 15.0f;
                bSizeFaster = true;
            }

            if(tolower(keys_eng[i]) == 'z')
            {
                if(bSizeAmmersive) {
                    m_fWidth -= 32.5f;
                }

                if(ImGui::Button("SHIFT", ImVec2(m_fWidth + 120.0f, m_fHeight))) {
                    keyboard::pushShift();
                }

                ImGui::SameLine(0.0f, m_fSame);
            }

            if(tolower(keys_eng[i]) == 'a') {
                if(bSizeFaster) m_fWidth -= 15.0f;

                ImGui::SetCursorPosX(ImGui::GetCursorPosX() + 80.0f);
            }

            if(i > 9 && i < 18 && !bSizeAmmersive) 
            {
                m_fWidth += 32.5f;
                bSizeAmmersive = true;
            }

            if(ImGui::Button(key_buf, ImVec2(m_fWidth, m_fHeight))) {
                keyboard::addChar(keys_eng[i]);
            } 

            if(tolower(keys_eng[i]) == 'm')
            {
                ImGui::SameLine(0.0f, m_fSame);

                ImGui::Button("DEL", ImVec2(m_fWidth + 250.0f, m_fHeight));
                if(ImGui::IsItemHovered()) {
                    keyboard::removeChar();
                }
            }

            if(tolower(keys_eng[i]) != 'p' && tolower(keys_eng[i]) != 'l' && tolower(keys_eng[i]) != 'm') {
                ImGui::SameLine(0.0f, m_fSame);
            }
        }
    }
    else if(keyboard::g_iType == KEYBOARD_TYPE_NUM)
    {
        if(keyboard::g_bSpecialSym == true)
        {
            for (int i = 0; i < sizeof(keys_num_special); i++)
            {
                if(!keys_num_special[i]) {
                    continue;
                }

                float fButtonSizeX = io.DisplaySize.x / 11;
                m_fWidth = fButtonSizeX;

                char key_buf[0xFF] = { 0 };
                sprintf(key_buf, "%c", keys_num_special[i]);

                if(ImGui::Button(key_buf, ImVec2(m_fWidth, m_fHeight))) {
                    keyboard::addChar(keys_num_special[i]);
                } 

                if(keys_num_special[i] == ']')
                {
                    ImGui::Button("TEST", ImVec2(m_fWidth, m_fHeight));
                }

                if(keys_num_special[i] != '\\' && keys_num_special[i] != ']') {
                    ImGui::SameLine(0.0f, m_fSame);
                }
            }
        }
        else
        {
            for (int i = 0; i < sizeof(keys_num); i++)
            {
                if(!keys_num[i]) {
                    continue;
                }

                float fButtonSizeX = io.DisplaySize.x / 11;
                m_fWidth = fButtonSizeX;

                char key_buf[0xFF] = { 0 };
                sprintf(key_buf, "%c", keys_num[i]);

                if(keys_num[i] == '*')
                {
                    if(ImGui::Button("=\\<", ImVec2(m_fWidth + 120.0f, m_fHeight))) {
                        keyboard::g_bSpecialSym ^= true;
                    }

                    ImGui::SameLine(0.0f, m_fSame);
                }

                if(ImGui::Button(key_buf, ImVec2(m_fWidth, m_fHeight))) {
                    keyboard::addChar(keys_num[i]);
                } 

                if(keys_num[i] == '?')
                {
                    ImGui::SameLine(0.0f, m_fSame);

                    ImGui::Button("DEL", ImVec2(m_fWidth + 250.0f, m_fHeight));
                    if(ImGui::IsItemHovered()) {
                        keyboard::removeChar();
                    }
                }

                if(keys_num[i] != '0' && keys_num[i] != '/' && keys_num[i] != '?') {
                    ImGui::SameLine(0.0f, m_fSame);
                }
            }
        }
    }
    else if(keyboard::g_iType == KEYBOARD_TYPE_RUS)
    {
        keyboard::g_iType = KEYBOARD_TYPE_ENG;
    }

    if(keyboard::g_bSpecialSym)
    {
        if(ImGui::Button("=\\<", ImVec2(m_fWidth + 120.0f, m_fHeight))) {
            keyboard::g_bSpecialSym ^= true;
        } 
    
        ImGui::SameLine(0.0f, m_fSame);
    
        if(ImGui::Button(",", ImVec2(m_fWidth, m_fHeight))) {
            keyboard::addChar(',');
        } 
    
        ImGui::SameLine(0.0f, m_fSame);
    
        ImGui::Button("DEL", ImVec2(m_fWidth, m_fHeight));
        if(ImGui::IsItemHovered()) {
            keyboard::removeChar();
        }
    
        ImGui::SameLine(0.0f, m_fSame);
    
        if(ImGui::Button("\t\t\t\t\t\t\t\t\t", ImVec2(m_fWidth + 670.0f, m_fHeight))) { // space
            keyboard::addChar(' ');
        } 
    
        ImGui::SameLine(0.0f, m_fSame);
    
        if(ImGui::Button(".", ImVec2(m_fWidth, m_fHeight))) {
            keyboard::addChar('.');
        } 
    
        if(bHistoryMessagesAllow)
        {
            ImGui::SameLine(0.0f, m_fSame);
    
            if(ImGui::Button("?", ImVec2(m_fWidth - 120.0f, m_fHeight))) 
            {
                keyboard::scrollUpHistory();
            }
    
            ImGui::SameLine(0.0f, m_fSame);
    
            if(ImGui::Button("?", ImVec2(m_fWidth - 120.0f, m_fHeight))) 
            {
                keyboard::scrollDownHistory();
            }
        }
    
        ImGui::SameLine(0.0f, m_fSame);
    
        if(ImGui::Button("SEND", ImVec2(bHistoryMessagesAllow == false ? m_fWidth + 230.0f : m_fWidth + 40.0f, m_fHeight))) 
        {
            if(bHistoryMessagesAllow) {
                keyboard::saveHistory(std::string(keyboard::getText()));
            }
    
            keyboard::setVisibility(false);
        } 
    }
    else
    {
        if(ImGui::Button("?123", ImVec2(m_fWidth + 120.0f, m_fHeight))) {
            keyboard::g_iType = keyboard::g_iType == KEYBOARD_TYPE_NUM ? m_lastType : KEYBOARD_TYPE_NUM;
        } 

        ImGui::SameLine(0.0f, m_fSame);

        if(ImGui::Button(",", ImVec2(m_fWidth, m_fHeight))) {
            keyboard::addChar(',');
        } 

        ImGui::SameLine(0.0f, m_fSame);

        if(ImGui::Button("LANG", ImVec2(m_fWidth, m_fHeight))) 
        {
            keyboard::g_iType = keyboard::g_iType == KEYBOARD_TYPE_ENG ? KEYBOARD_TYPE_RUS : KEYBOARD_TYPE_ENG;
            m_lastType = keyboard::g_iType;
        } 

        ImGui::SameLine(0.0f, m_fSame);

        if(ImGui::Button("\t\t\t\t\t\t\t\t\t", ImVec2(m_fWidth + 670.0f, m_fHeight))) { // space
            keyboard::addChar(' ');
        } 

        ImGui::SameLine(0.0f, m_fSame);

        if(ImGui::Button(".", ImVec2(m_fWidth, m_fHeight))) {
            keyboard::addChar('.');
        } 

        if(bHistoryMessagesAllow)
        {
            ImGui::SameLine(0.0f, m_fSame);

            if(ImGui::Button("?", ImVec2(m_fWidth - 120.0f, m_fHeight))) 
            {
                keyboard::scrollUpHistory();
            }
    
            ImGui::SameLine(0.0f, m_fSame);
    
            if(ImGui::Button("?", ImVec2(m_fWidth - 120.0f, m_fHeight))) 
            {
                keyboard::scrollDownHistory();
            }
        }

        ImGui::SameLine(0.0f, m_fSame);

        if(ImGui::Button("SEND", ImVec2(bHistoryMessagesAllow == false ? m_fWidth + 230.0f : m_fWidth + 40.0f, m_fHeight))) 
        {
            if(bHistoryMessagesAllow) {
                keyboard::saveHistory(std::string(keyboard::getText()));
            }

            keyboard::setVisibility(false);
        } 
    }

    ImGui::SetWindowSize(ImVec2(io.DisplaySize.x, io.DisplaySize.y));
	ImVec2 size = ImGui::GetWindowSize();
	ImGui::SetWindowPos( ImVec2(0, io.DisplaySize.y * (1-0.55)) );
	ImGui::End();
}