/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "gui.h"
#include "window.h"
#include "imguiwrapper.h"

void InitGUI()
{
    // setup ImGUI
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();

	ImGuiWrapper::Init();

	// setup style
	ImGuiStyle& style = ImGui::GetStyle();
	style.ScrollbarSize = 55;
	style.WindowBorderSize = 0.0f;
	ImGui::StyleColorsDark();
	style.WindowRounding = 0.0f;
}

void DestroyGUI()
{
    ImGuiWrapper::ShutDown();
    ImGui::DestroyContext();
}