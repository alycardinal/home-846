/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "../core/core.h"
#include "imguiwrapper.h"
#include "button.h"

#define BUTTON_TOUCH_NONE       0
#define BUTTON_TOUCH_HOVERED    1
#define BUTTON_TOUCH_ACTIVE     2

button::button(uint32_t id, const char* text, float x, float y, float sx, float sy, ImColor backgroundColor, ImColor textColor, bool bShow)
{
    strcpy(g_text, text);

    g_fPosX = x;
    g_fPosY = y;
    g_fSizeX = sx;
    g_fSizeY = sy;
    g_bgColor = backgroundColor;
    g_textColor = textColor;
    g_bShow = bShow;
    g_ID = id;
    g_iButtonTouchEx = BUTTON_TOUCH_NONE;
}

button::~button()
{
    g_fPosX = 0.0f;
    g_fPosY = 0.0f;
    g_fSizeX = 0.0f;
    g_fSizeY = 0.0f;
    g_bgColor = 0;
    g_textColor = 0;
    g_bShow = false;
    g_ID = 0;
    g_iButtonTouchEx = BUTTON_TOUCH_NONE;
}

bool button::checkIn(float x, float y)
{
    return (x < this->g_fPosX || y < this->g_fPosY ||
            x > (this->g_fPosX + this->g_fSizeX) || y > (this->g_fPosY + this->g_fSizeY)) ? false : true;
}

bool button::isHovered()
{
    return this->g_iButtonTouchEx;
}

void button::draw(window* win)
{
    if(!g_bShow || !win || !g_text) {
        return;
    }

    ImGuiIO& io = ImGui::GetIO();

    ImVec2 vecInfoPos = ImVec2(g_fPosX, g_fPosY);
    ImVec2 vecInfoSize = ImVec2(g_fPosX + g_fSizeX, g_fPosY + g_fSizeY);

    win->fill(vecInfoPos, vecInfoSize, g_iButtonTouchEx == BUTTON_TOUCH_HOVERED ? ImColor(123, 123, 123, 255) : g_bgColor);

    ImVec2 vecTextPos = ImVec2((g_fPosX + g_fSizeX/2) - ImGui::CalcTextSize(g_text).x/2, (g_fPosY + g_fSizeY/2) - ImGui::CalcTextSize(g_text).y/2);
    win->addText(30.0f, win->getFont(), vecTextPos, g_textColor, true, g_text);
}

void button::pushTouch(float x, float y)
{
    if(!button::checkIn(x, y)) {
        g_iButtonTouchEx = BUTTON_TOUCH_NONE;
        return;
    }

    g_iButtonTouchEx = BUTTON_TOUCH_HOVERED;
}

void button::popTouch(float x, float y)
{
    g_iButtonTouchEx = BUTTON_TOUCH_NONE;
}

void button::moveTouch(float x, float y)
{
    if(!button::checkIn(x, y)) {
        g_iButtonTouchEx = BUTTON_TOUCH_NONE;
        return;
    }

    g_iButtonTouchEx = BUTTON_TOUCH_HOVERED;
}